/*
	Name: Perimetro del triangulo equilatero
	Copyright: asdff
	Author: Luis Fernando Reynoso 
	Date: 14/08/20 21:51
	Description: 
*/

#include<stdio.h>

int main(){
	int lado;
	printf("Perimetro del triangulo equilatero\n");
	printf("Dame el valor de uno de los lados del triangulo equilatero\n");
	scanf("%d",&lado);
	lado=lado*3;
	printf("el perimetro de tu triangulo es: %d",lado);
	return 0;
}
