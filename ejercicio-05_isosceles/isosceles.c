/*
	Name: Perimetro del triangulo isosceles
	Copyright: asdff
	Author: Luis Fernando Reynoso 
	Date: 14/08/20 21:51
	Description: 
*/

#include<stdio.h>

int main(){
	int lados, base, perimetro;
	printf("Perimetro del triangulo isosceles\n");
	printf("Dame el valor de uno de los dos lados iguales del triangulo \n");
	scanf("%d",&lados);
	printf("Dame el valor de la base del triangulo \n");
	scanf("%d",&base);
	perimetro=(lados*2)+base;
	printf("el perimetro de tu triangulo es: %d",perimetro);
	return 0;
}
