/*
	Name: Perimetro del triangulo escaleno
	Copyright: asdff
	Author: Luis Fernando Reynoso 
	Date: 14/08/20 21:51
	Description: 
*/

#include<stdio.h>

int main(){
	int lado1, lado2, lado3, perimetro;
	printf("Perimetro del triangulo escaleno\n");
	printf("Dame el valor del primer lado del triangulo \n");
	scanf("%d",&lado1);
	printf("Dame el valor de el segundo lado del triangulo \n");
	scanf("%d",&lado2);
	printf("Dame el valor de el tercer lado del triangulo \n");
	scanf("%d",&lado3);
	perimetro=lado1+lado2+lado3;
	printf("el perimetro de tu triangulo es: %d",perimetro);
	return 0;
}
